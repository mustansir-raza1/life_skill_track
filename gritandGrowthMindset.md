### Introduction to Grit.
Grit, defined as passion and perseverance for long-term goals, is a significant predictor of success in many ways. However, there is still limited knowledge on how to build grit in individuals, especially in children. One of the best approach to build grit is promoting a growth mindset.
### Fixed Mindset vs Growth Mindset
- Growth Mindset:
    - people who are good at something  are good because they built that ability, and people who aren't are not good because they haven't done the work.
    - believe that skills  and intelligence are grown and developed.
    - believe  that you are in control of your abilities.

- Fixed Mindset: 
    - believe that skills  and intelligence are set  and you either have them or you don't,
    - that some people are just naturally  good at things, while others are not.
    - believe that you are not in control  of your abilities.

### What is the Internal Locus of Control?
- Internal Focus of control: 
    - Belief that your effort and actions determine your outcomes.
    - Focus on the factors which are under your control.
    - Recognizing how your actions solve problems in your life.
- key points
    - Students who were praised for their intelligence showed lower motivation and believed that external factors determined their success. 
    - The study found that students who were praised for their effort and hard work instead of their intelligence showed higher levels of motivation. 
    
    - Taking credit for the positive outcomes you achieve.
    - Developing an internal locus of control is key to staying motivated.
### How to build a Growth Mindset?
- develop your own life curriculum
- question your assumptions
- believe in your ability to figure things out
- Honoring the struggle
### What are your ideas to take action and build Growth Mindset?
- Embrace Challenges:-
  - See them as opportunities.
- Learn from Failures:
  - They're chances to improve.
- Value Effort:
  - Hard work leads to growth.
- Use "Yet":-
  - Acknowledge progress and potential.
- Seek Feedback:-
  - Learn from others' perspectives.
- Celebrate Progress:-
  -  Every step forward counts.
- Be Curious: 
  - Explore and ask questions.
- Lead by Example:-
  - Show a growth mindset in action.